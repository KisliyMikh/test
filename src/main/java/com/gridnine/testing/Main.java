package com.gridnine.testing;

public class Main {
    public static void main(String[] args) {
        System.out.println(FlightBuilder.createFlights());
        Filter filter = new Filter(FlightBuilder.createFlights());
        System.out.println(filter.departureBeforeTime());
        System.out.println(filter.segmentArrivalBeforeDeparture());
        System.out.println(filter.longTimeOnEarth());
    }
}
