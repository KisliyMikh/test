package com.gridnine.testing;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Filter {

    private final List<Flight> flights;

    public Filter(List<Flight> flights) {
        this.flights = flights;
    }

    public List<Flight> departureBeforeTime() {
        List<Flight> flights1 = new ArrayList<>();
        for (Flight fl : flights) {
            if (fl.getSegments()
                    .stream()
                    .anyMatch(segment -> segment.getDepartureDate()
                            .isAfter(LocalDateTime.now()))) {
                flights1.add(fl);
            }
        }
        return flights1;

    }

    public List<Flight> segmentArrivalBeforeDeparture() {
        List<Flight> flights1 = new ArrayList<>();
        for (Flight fl : flights) {
            if (fl.getSegments()
                    .stream()
                    .anyMatch(segment -> segment.getArrivalDate()
                            .isAfter(segment.getDepartureDate()))) {
                flights1.add(fl);
            }
        }
        return flights1;
    }

    public List<Flight> longTimeOnEarth() {
        List<Flight> flightList = new ArrayList<>();
        for (Flight fl : flights) {
            List<Segment> segmentList = fl.getSegments();
            int timeOnEarth = 0;
            for (int i = 0; i + 1 < segmentList.size(); i++) {
                Duration duration = Duration.between(segmentList.get(i).getArrivalDate(), segmentList.get(i + 1).getDepartureDate());
                timeOnEarth += duration.toHoursPart();
            }
            if (timeOnEarth < 2 && segmentList.size() > 1) {
                flightList.add(fl);
            }
        }
        return flightList;
    }
}